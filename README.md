# tempalates-namespaces-desa

Generar roles, cuotas y self-service para la administración de forma autónoma


## Tareas ##



### Criterios de desuso de PODS



### Template de qutas en desarrollo

Asignar cuota base para el namespace

### ResourceQuota
```bash
apiVersion: v1
kind: ResourceQuota
metadata:
  name: <nombre-resourcequota>
  namespace: <nombre-namespace>
spec:
  hard:
    pods: '4'
    requests.cpu: '1'
    requests.memory: 1Gi
    limits.cpu: '2'
    limits.memory: 2Gi

```

### LimitRange
```yaml
apiVersion: v1
kind: LimitRange
metadata:
  name: <nombre-limitrange>
  namespace: <nombre-namespace>
spec:
  limits:
    - default:
        memory: 512Mi
      defaultRequest:
        memory: 256Mi
      type: Container
```

### funcionalidades


### scripts

- scripts de gestión del limite de quota.

- scripts de gestión del limit range.

- scripts maestro para la gestión de templates en namespaces de Desarrollo.

